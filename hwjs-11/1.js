document.onkeypress = function(event) {
    const key = event.key.toLowerCase();
    const buttons = document.querySelectorAll(".btn-wrapper > .btn");
    for (let btn of buttons) {
      if (btn.innerText.toLowerCase() === key) btn.classList.add("downKeybord")
      else btn.classList.remove("downKeybord");
    }
  }