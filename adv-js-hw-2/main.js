let root = document.getElementById("root");
const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
];
function displayBooks(books) {
  let ul = document.createElement("ul");
  root.append(ul);
  for (var i = 0; i < books.length; i++) {
    try {
      if(books[i].author==null){
        throw "Book "+(i+1)+" is missing author";
      }
      if(books[i].name==null){
        throw "Book "+(i+1)+" is missing name";
      }
      if(books[i].price==null){
        throw "Book "+(i+1)+" is missing price";
      }
      let book = document.createElement("li");
      ul.append(book);
      book.innerHTML = books[i].author + " \"" + books[i].name + "\" " + " $"+books[i].price;
    } catch(e){
      console.log(e);
    }
  }
}
displayBooks(books);