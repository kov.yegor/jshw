class Employee{
    constructor(name,age,salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName() {
        return this.name;
    }
    getAge(){
        return this.age;
    }
    getSalary(){
        return this.age;
    }
    setName(name){
        this.name = name;
    }
    setAge(age){
        this.age = age;
    }
    setSalary(salary){
        this.salary = salary;
    }
}
class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary);
        this.lang = lang;
    }
    getSalary(){
        return this.salary*3;
    }
    getLang(){
        return this.lang;
    }
    setLang(lang){
        this.lang = lang;
    }
}

let programmer1 = new Programmer("Alan", 24, 60,["JavaScript","Java","Python","C++"]);
let programmer2 = new Programmer("Bob", 20, 30,["Java","JavaScript"]);
let programmer3 = new Programmer("Sasha", 23, 15,["C#","C++"]);
console.log(programmer1);
console.log(programmer1.getSalary());
console.log(programmer2);
console.log(programmer2.getSalary());
console.log(programmer3);
console.log(programmer3.getSalary());